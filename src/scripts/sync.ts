#!/usr/bin/env node
import { generate } from '@graphql-codegen/cli'
import { join } from 'path'
import { mkdirSync, readFileSync, writeFileSync } from 'fs'
import { render } from 'mustache'

const mustacheTemplatesPath = join( __dirname, '..', 'utils', 'templates')
const servicesPath: string = join( __dirname, '..', 'client', 'services' )
const apolloPath: string = join( __dirname, '..', 'client', 'apollo' )
const defaultOperationsPath: string = join( __dirname, '..', 'client', 'operations' )

/**
 * This function generates defaults queries and mutations as graphql document node type (gql)
 */
const generateDefaultGqlOperations = ( operations: Record<string, any>): void => {
    mkdirSync( defaultOperationsPath, { recursive: true } )
    const queriesTemplate: string = readFileSync( join( mustacheTemplatesPath, 'defaultQueries.mustache' ) ).toString()
    const mutationsTemplate: string = readFileSync( join( mustacheTemplatesPath, 'defaultMutations.mustache' ) ).toString()
    writeFileSync( join( defaultOperationsPath, 'defaultQueries.ts' ), render( queriesTemplate, {
        queries: operations.queries
    } ) )
    writeFileSync( join( defaultOperationsPath, 'defaultMutations.ts' ), render( mutationsTemplate, {
        mutations: operations.mutations
    } ) )
}

/**
 * This function generates typescript files of queries service (queriesService.ts) using mustache templates
 * @param queries Queries
 */
const createQueriesService = ( queries: Record<string, any> ) => {
    const queriesTemplatePath: string = join( mustacheTemplatesPath, 'queriesService.mustache' )
    const queriesServiceTemplate: string = readFileSync( queriesTemplatePath ).toString()
    writeFileSync( join( servicesPath, `queriesService.ts` ), render( queriesServiceTemplate, {
        queries
    } ) )
}

/**
 * Check if a field is scalar or object
 * This is for include only scalars fields on defaults queries and exclude upper level queries (nested queries)
 * @param fieldType
 */
const isScalarField = ( fieldType: Record<string, any>): boolean => {
    const { kind, ofType } = fieldType
    if ( kind === 'NON_NULL') {
        return isScalarField( ofType )
    } else if (kind === 'SCALAR') {
        return true
    }
    return false
}

const createMutationsService = ( mutations: Record<string, any> ) => {
    const mutationsTemplatePath: string = join( mustacheTemplatesPath, 'mutationsService.mustache' )
    const mutationsServiceTemplate: string = readFileSync( mutationsTemplatePath ).toString()
    writeFileSync( join( servicesPath, `mutationsService.ts` ), render( mutationsServiceTemplate, {
        mutations
    } ) )
}

const createIndex = (): void => {
    const scalarsClientTemplate: string = readFileSync(
        join( mustacheTemplatesPath, 'index.mustache' )
    ).toString()
    writeFileSync( join( __dirname, '..', 'ScalarsClient.ts'), render( scalarsClientTemplate, { } ) )
}

const createApolloClient = (): void => {
    const apolloClientTemplate: string = readFileSync(
        join( mustacheTemplatesPath, 'ApolloClient.mustache' )
    ).toString()
    writeFileSync( join( apolloPath, 'ApolloClient.ts'), render( apolloClientTemplate, { } ) )
}

const createApolloClientService = () => {
    const apolloClientServiceTemplate: string = readFileSync(
        join( mustacheTemplatesPath, 'ApolloClientService.mustache' )
    ).toString()
    writeFileSync( join( apolloPath, 'ApolloClientService.ts'), render( apolloClientServiceTemplate, {} ) )
}

const createApolloClientManager = () => {
    const apolloClientManagerTemplate: string = readFileSync(
        join( mustacheTemplatesPath, 'ApolloClientManager.mustache' )
    ).toString()
    writeFileSync( join( apolloPath, 'ApolloClientManager.ts'), render( apolloClientManagerTemplate, {} ) )
}

const createServicesByOperations = ( operations: Record<string, any> ) => {
    mkdirSync( servicesPath, { recursive: true } )
    createQueriesService( operations.queries )
    createMutationsService( operations. mutations )
}

const getOperationType = ( queryType: Record<string, any> ): string => {
    const { kind, name, ofType } = queryType
    if ( /NON_NULL|LIST/gm.test( kind ) ) {
        return getOperationType( ofType )
    }
    else if ( /OBJECT/gm.test( kind ) ) {
        return name
    }
    return ''
}

const getQueries = ( entity: Record<string, any>, queries: Array<Record<string, any>> ): Array<Record<string, any>> => {
    const entityQueries: Array<Record<string, any>> = []
    queries.forEach( ( query: Record<string, any > ) => {
        if ( getOperationType( query.type ) === entity.name ) {
            entityQueries.push( {
                operation: query.name,
                _operation: query.name.charAt(0).toUpperCase().concat( query.name.slice(1) ),
                entity: entity.name,
                args: query.args.map( ( arg: Record<string, any> ) => {
                    const { type: { kind, name, ofType } } = arg
                    return {
                        name: arg.name,
                        required: /NON_NULL/gm.test( kind ),
                        type: /NON_NULL/gm.test( kind ) ? ofType.name : name
                    }
                } ),
                fields: entity.fields
                    .filter( ( field: Record<string, any> ) => isScalarField( field.type ) )
                    .map( ( field: Record<string, any> ) => field.name )
            } )
        }
    } )
    return entityQueries
}

const getMutations = ( entity: Record<string, any>, mutations: Array<Record<string, any>> ): Array<Record<string, any>> => {
    const entityMutations: Array<Record<string, any>> = []
    mutations.forEach( ( mutation: Record<string, any> ) => {
        if ( getOperationType( mutation.type ) === entity.name ) {
            entityMutations.push( {
                operation: mutation.name,
                _operation: mutation.name.charAt(0).toUpperCase().concat( mutation.name.slice(1) ),
                entity: entity.name,
                args: mutation.args.map( ( arg: Record<string, any> ) => {
                    const { type: { kind, name, ofType } } = arg
                    return {
                        name: arg.name,
                        required: /NON_NULL/gm.test( kind ),
                        type: /NON_NULL/gm.test( kind ) ? ofType.name : name
                    }
                } ),
                fields: entity.fields
                    .filter( ( field: Record<string, any> ) => isScalarField( field.type ) )
                    .map( ( field: Record<string, any> ) => field.name )
            } )
        }
    } )
    return entityMutations
}

const getServicesByOperations = ( objects: Array<Record<string, any>> ): Record<string, any> => {
    const operations: Record<string, any> = {
        queries: [],
        mutations: []
    }
    const queryObject: Record<string, any> | undefined = objects
        .filter( ( object: Record<string, any> ) =>
            /^Query$/gm.test( object.name ) &&
            !/^.+Connection$/gm.test( object.name )
        ).shift()
    const mutationObject: Record<string, any> | undefined = objects
        .filter( ( object: Record<string, any> ) =>
            /^Mutation$/gm.test( object.name ) &&
            !/^.+Connection$/gm.test( object.name )
        ).shift()
    const entitiesObjects: Array<Record<string, any>> = objects
        .filter( ( object: Record<string, any> ) =>
            !/^Mutation$/gm.test( object.name ) &&
            !/^Query$/gm.test( object.name ) &&
            !/^.+Connection$/gm.test( object.name )
        )
    entitiesObjects.forEach( ( entity: Record<string, any> ) => {
        operations.queries.push( ...getQueries( entity, queryObject?.fields ) )
        operations.mutations.push( ...getMutations( entity, mutationObject?.fields ) )
    } )
    return operations
}

const generateTypedSchema = async (): Promise<void> => {
    await generate( {
        generates: {
            [join( __dirname, '..', 'client', 'types.d.ts' )]: {
                schema: 'https://app.scalars.co/eg2bvy9x4b/api',
                plugins: ['typescript', 'typescript-operations'],
                documents: [
                    join( defaultOperationsPath, 'defaultQueries.ts' ),
                    join( defaultOperationsPath, 'defaultMutations.ts' )
                ]
            }
        }
        // TODO Add another generates: {...} for custom queries specified at scalars.yaml
    }, true )
}

const introspect = async (): Promise<Array<Record<string, any>>> => {
    const { "0": { content } } = await generate( {
        generates: {
            [join( __dirname, 'introspection.json' )]: {
                schema: 'https://app.scalars.co/eg2bvy9x4b/api',
                plugins: ['introspection'],
                config: {
                    minify: false,
                    descriptions: true,
                    schemaDescription: true
                }
            }
        }
    }, false )
    const { '__schema': { types } }: Record<string, any> = JSON.parse( content )
    return types
        .filter( ( type: any ) => /^OBJECT$/gm.test( type.kind ) && !/^__.+/gm.test( type.name ) )
}

const generateApolloClient = ( operations: Record<string, any> ):void => {
    mkdirSync( apolloPath, { recursive: true } )
    createServicesByOperations( operations )
    createApolloClientService()
    createApolloClientManager()
    createApolloClient()
    createIndex()
}

(async function main () {
    // Primero la instrospeccion
    const introspection: Array<Record<string, any>> = await introspect()
    // Generar el custom json de la introspeccion
    const operations: Record<string, any> = getServicesByOperations( introspection )
    // Generar los default operations (queries y mutaciones por defecto)
    generateDefaultGqlOperations( operations )
    // Generar types.d.ts
    await generateTypedSchema()
    // Crear archivos con mustache (apollo client en si)
    generateApolloClient( operations )
})()

