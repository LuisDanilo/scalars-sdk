import { ApolloClientService } from '../apollo/ApolloClientService'

export class MutationsService extends ApolloClientService {
    createDummyString() {
        return 'Holo from create dummy string'
    }
}
