import { ApolloClientService } from '../apollo/ApolloClientService'

export class QueriesService extends ApolloClientService {
    getDummyString() {
        return 'Holo dummy string from queries service'
    }
}
