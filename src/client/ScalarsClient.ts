import { ApolloClient } from './apollo/ApolloClient'
import { ApolloClientManager } from './apollo/ApolloClientManager'
import { ApolloError as ScalarsClientError } from '@apollo/client'

export class ScalarsClient extends ApolloClient {
    constructor() {
        // Very first introspection
        // introspect().then( () => {
        //     console.log("Introspection finished ❤️❤️❤️")
        // })
        // Apollo client
        const apolloClientManager = new ApolloClientManager(
            'https://app.scalars.co/eg2bvy9x4b/api',
            'client_id 1dc47a10-af42-11eb-8847-b1b62795d90c'
        )
        super( apolloClientManager )
    }
}

export { ScalarsClientError }
