import { generate } from "@graphql-codegen/cli";
import { join } from "path";
import { readFileSync, unlinkSync, writeFileSync } from "fs";
import { render } from "mustache";
import ts, { ModuleKind, ScriptTarget } from 'typescript'

const mustacheTemplatesPath = join( __dirname, 'templates' )
const defaultOperationsPath: string = join( __dirname, '..', 'client', 'operations' )
const servicesPath: string = join( __dirname, '..', 'client', 'services' )

// https://github.com/Microsoft/TypeScript/wiki/Using-the-Compiler-API
function compile(fileNames: Array<string>, options: ts.CompilerOptions): void {
    // Create a Program with an in-memory emit
    const createdFiles: Record<string, any> = {}
    const host = ts.createCompilerHost(options);
    host.writeFile = (fileName: string, contents: string) => createdFiles[fileName] = contents
    // Prepare and emit the d.ts files
    const program = ts.createProgram(fileNames, options, host);
    program.emit();
    Object.entries(createdFiles).forEach( entry => {
        writeFileSync( entry[0], entry[1],
            // ( err ) => {
            //     if (err) {
            //         console.error(err)
            //     }
            //     console.log(`File ${entry[0]} created`)
            // }
        )
    })
}

const createMutationsService = ( mutations: Record<string, any> ) => {
    const mutationsTemplatePath: string = join( mustacheTemplatesPath, 'mutationsService.mustache' )
    const mutationsServiceTemplate: string = readFileSync( mutationsTemplatePath ).toString()
    writeFileSync(
        join( servicesPath, `mutationsService.ts` ),
        render( mutationsServiceTemplate, { mutations } ),
        // ( err ) => {
        //     if ( err ) {
        //         console.error( err )
        //     }
        // }
    )
}

const createQueriesService = ( queries: Record<string, any> ) => {
    const queriesTemplatePath: string = join( mustacheTemplatesPath, 'queriesService.mustache' )
    const queriesServiceTemplate: string = readFileSync( queriesTemplatePath ).toString()
    writeFileSync(
        join( servicesPath, `queriesService.ts` ),
        render( queriesServiceTemplate, { queries } ),
        // ( err ) => {
        //     if ( err ) {
        //         console.error( err )
        //     }
        // }
    )
}

const generateTypedSchema = async (): Promise<void> => {
    await generate( {
        generates: {
            [join( __dirname, '..', 'client', 'types.ts' )]: {
                schema: 'https://app.scalars.co/eg2bvy9x4b/api',
                plugins: ['typescript', 'typescript-operations'],
                documents: [
                    join( defaultOperationsPath, 'defaultQueries.ts' ),
                    join( defaultOperationsPath, 'defaultMutations.ts' )
                ]
            }
        }
        // TODO Add another generates: {...} for custom queries specified at scalars.yaml
    }, true )
    compile([
        join( __dirname, '..', 'client', 'types.ts' )
    ], {
        target: ScriptTarget.ES5,
        module: ModuleKind.CommonJS,
        declaration: true,
        declarationMap: true,
        sourceMap: true,
        outDir: join( __dirname, '..', 'client'),
        strict: true,
        esModuleInterop: true,
        skipLibCheck: true,
        forceConsistentCasingInFileNames: true
    })
}

const generateDefaultGqlOperations = ( operations: Record<string, any>): void => {
    const queriesTemplate: string = readFileSync( join( mustacheTemplatesPath, 'defaultQueries.mustache' ) ).toString()
    const mutationsTemplate: string = readFileSync( join( mustacheTemplatesPath, 'defaultMutations.mustache' ) ).toString()
    writeFileSync(
        join( defaultOperationsPath, 'defaultQueries.ts' ),
        render( queriesTemplate, { queries: operations.queries } ),
        // ( err ) => {
        //     if ( err ) {
        //         console.error(err)
        //     }
        // }
    )
    writeFileSync(
        join( defaultOperationsPath, 'defaultMutations.ts' ),
        render( mutationsTemplate, { mutations: operations.mutations } ),
        // ( err ) => {
        //     if ( err ) {
        //         console.error(err)
        //     }
        // }
    )
    compile([
        join( defaultOperationsPath, 'defaultQueries.ts' ),
        join( defaultOperationsPath, 'defaultMutations.ts' )
    ], {
        target: ScriptTarget.ES5,
        module: ModuleKind.CommonJS,
        declaration: true,
        declarationMap: true,
        sourceMap: true,
        outDir: defaultOperationsPath,
        strict: true,
        esModuleInterop: true,
        skipLibCheck: true,
        forceConsistentCasingInFileNames: true
    })
}

const isScalarField = ( fieldType: Record<string, any>): boolean => {
    const { kind, ofType } = fieldType
    if ( kind === 'NON_NULL') {
        return isScalarField( ofType )
    } else if (kind === 'SCALAR') {
        return true
    }
    return false
}

const getOperationType = ( queryType: Record<string, any> ): string => {
    const { kind, name, ofType } = queryType
    if ( /NON_NULL|LIST/gm.test( kind ) ) {
        return getOperationType( ofType )
    }
    else if ( /OBJECT/gm.test( kind ) ) {
        return name
    }
    return ''
}

const getMutations = ( entity: Record<string, any>, mutations: Array<Record<string, any>> ): Array<Record<string, any>> => {
    const entityMutations: Array<Record<string, any>> = []
    mutations.forEach( ( mutation: Record<string, any> ) => {
        if ( getOperationType( mutation.type ) === entity.name ) {
            entityMutations.push( {
                operation: mutation.name,
                _operation: mutation.name.charAt(0).toUpperCase().concat( mutation.name.slice(1) ),
                entity: entity.name,
                args: mutation.args.map( ( arg: Record<string, any> ) => {
                    const { type: { kind, name, ofType } } = arg
                    return {
                        name: arg.name,
                        required: /NON_NULL/gm.test( kind ),
                        type: /NON_NULL/gm.test( kind ) ? ofType.name : name
                    }
                } ),
                fields: entity.fields
                    .filter( ( field: Record<string, any> ) => isScalarField( field.type ) )
                    .map( ( field: Record<string, any> ) => field.name )
            } )
        }
    } )
    return entityMutations
}

const getQueries = ( entity: Record<string, any>, queries: Array<Record<string, any>> ): Array<Record<string, any>> => {
    const entityQueries: Array<Record<string, any>> = []
    queries.forEach( ( query: Record<string, any > ) => {
        if ( getOperationType( query.type ) === entity.name ) {
            entityQueries.push( {
                operation: query.name,
                _operation: query.name.charAt(0).toUpperCase().concat( query.name.slice(1) ),
                entity: entity.name,
                args: query.args.map( ( arg: Record<string, any> ) => {
                    const { type: { kind, name, ofType } } = arg
                    return {
                        name: arg.name,
                        required: /NON_NULL/gm.test( kind ),
                        type: /NON_NULL/gm.test( kind ) ? ofType.name : name
                    }
                } ),
                fields: entity.fields
                    .filter( ( field: Record<string, any> ) => isScalarField( field.type ) )
                    .map( ( field: Record<string, any> ) => field.name )
            } )
        }
    } )
    return entityQueries
}

const getServicesByOperations = ( objects: Array<Record<string, any>> ): Record<string, any> => {
    const operations: Record<string, any> = {
        queries: [],
        mutations: []
    }
    const queryObject: Record<string, any> | undefined = objects
        .filter( ( object: Record<string, any> ) =>
            /^Query$/gm.test( object.name ) &&
            !/^.+Connection$/gm.test( object.name )
        ).shift()
    const mutationObject: Record<string, any> | undefined = objects
        .filter( ( object: Record<string, any> ) =>
            /^Mutation$/gm.test( object.name ) &&
            !/^.+Connection$/gm.test( object.name )
        ).shift()
    const entitiesObjects: Array<Record<string, any>> = objects
        .filter( ( object: Record<string, any> ) =>
            !/^Mutation$/gm.test( object.name ) &&
            !/^Query$/gm.test( object.name ) &&
            !/^.+Connection$/gm.test( object.name )
        )
    entitiesObjects.forEach( ( entity: Record<string, any> ) => {
        operations.queries.push( ...getQueries( entity, queryObject?.fields ) )
        operations.mutations.push( ...getMutations( entity, mutationObject?.fields ) )
    } )
    return operations
}

const getIntrospectionFilteredByObjects = async (): Promise<Array<Record<string, any>>> => {
    const { "0": { content } } = await generate( {
        generates: {
            [join( __dirname, 'introspection.json' )]: {
                schema: 'https://app.scalars.co/eg2bvy9x4b/api',
                plugins: ['introspection'],
                config: {
                    minify: false,
                    descriptions: true,
                    schemaDescription: true
                }
            }
        }
    }, false )
    const { '__schema': { types } }: Record<string, any> = JSON.parse( content )
    return types
        .filter( ( type: any ) => /^OBJECT$/gm.test( type.kind ) && !/^__.+/gm.test( type.name ) )
}

export const introspect = async (): Promise<void> => {
    const objects: Array<Record<string, any>> = await getIntrospectionFilteredByObjects()
    const operations: Record<string, any> = getServicesByOperations( objects )
    generateDefaultGqlOperations( operations )
    await generateTypedSchema()
    createQueriesService( operations.queries )
    createMutationsService( operations. mutations )
    compile([
        join( servicesPath, `queriesService.ts` ),
        join( servicesPath, `mutationsService.ts` )
    ], {
        target: ScriptTarget.ES5,
        module: ModuleKind.CommonJS,
        declaration: true,
        declarationMap: true,
        sourceMap: true,
        outDir: servicesPath,
        strict: true,
        esModuleInterop: true,
        skipLibCheck: true,
        forceConsistentCasingInFileNames: true
    })
    // const removeFiles = [
    //     join( servicesPath, `queriesService.ts` ),
    //     join( servicesPath, `mutationsService.ts` ),
    //     join( __dirname, '..', 'client', 'types.ts' ),
    //     join( defaultOperationsPath, 'defaultQueries.ts' ),
    //     join( defaultOperationsPath, 'defaultMutations.ts' )
    //
    // ]
    // removeFiles.forEach( file => {
    //     unlinkSync(file)
    // })
}
