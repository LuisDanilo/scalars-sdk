import { FetchPolicy, ApolloClient, NormalizedCacheObject } from '@apollo/client'

interface MutationApolloOptions {
    fetchPolicy?: Extract<FetchPolicy, 'no-cache'>
}

interface ApolloOptions {
    fetchPolicy?: FetchPolicy
}

type FetchFunction = ( uri: string, options: any ) => Promise<any>;

export type TokenFunction = () => string;

export interface ScalarsClientConfig {
    endpoint: string
    clientId: string
}

export interface YamlParsedConfig {
    endpoint: string
    clientId: string
    queries: string | Array<string>
}

export enum ClientType {
    IMPLICIT= 'implicit',
    CODE = 'code'
}

export type ApolloClients = {
    [key in ClientType]: {
        client: ApolloClient<NormalizedCacheObject>|null;
        fetch?: FetchFunction;
        credentials?: string;
        token?: string;
    }
}

export interface MutationClientOptions {
    client?: ClientType
    apolloOptions?: MutationApolloOptions
}

export interface ClientOptions {
    client?: ClientType
    apolloOptions?: ApolloOptions
}
